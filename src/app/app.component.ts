import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'flex';

  finish : boolean = false;

  ngOnInit(){
    console.log('Init');
  }

  /*
  @HostListener("scroll", ['$event'])
  doSomethingOnInternalScroll($event:Event){
    console.log("scroll: ", event);
  }
  */


  @HostListener("scroll", ["$event"])
  doSomethingOnInternalScroll($event:Event) {
//In chrome and some browser scroll is given to body tag
let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
let max = document.documentElement.scrollHeight;

// pos/max will give you the distance between scroll bottom and and bottom of screen in percentage.
 if(pos > max && !this.finish)   {
  this.finish = true
  console.log("Final");

}else{
  this.finish = false;
}

  }


}//class
